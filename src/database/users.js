'use strict';

const Users = (sequelize, DataTypes) => {
	return sequelize.define('Users', {
		id: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing id'}}
		},
		username: {
			type: DataTypes.STRING,
            validate: {notEmpty: {msg: '-> Missing username'}},
            unique: true,
			allowNull: false
		},
		fullname: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing fullname'}},
			allowNull: false
		},
		email: {
			type: DataTypes.STRING,
            validate: {notEmpty: {msg: '-> Missing email'}},
			unique: true,
			allowNull: false
		},
		hashPassword: {
            type: DataTypes.STRING,
            validate: {notEmpty: {msg: '-> Missing hashPassword'}},
			allowNull: false
        },
        public: {
            type: DataTypes.BOOLEAN,
            validate: {notEmpty: {msg: '-> Missing public option'}},
            allowNull: false,
            defaultValue:  false
        }
	});
};

module.exports = Users;