'use strict';

const Songs = (sequelize, DataTypes) => {
	const songs = sequelize.define('Songs', {
		id: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing id'}}
		},
		title: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing title'}},
			allowNull: false
		},
		duration: {
            type: DataTypes.STRING,
            validate: {notEmpty: {msg: '-> Missing duration'}},
			allowNull: false
		},
		genre: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing genre'}},
			allowNull: false
		},
		artistId: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing artist'}},
			allowNull: false
		},
		diskId: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing disk'}},
			allowNull: false
		}
    });
    
    return songs;
};

module.exports = Songs;