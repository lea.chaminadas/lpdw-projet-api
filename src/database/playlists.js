'use strict';

const Playlists = (sequelize, DataTypes) => {
	const playlists = sequelize.define('Playlists', {
		id: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing id'}}
		},
		title: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing title'}},
			allowNull: false
		},
		userId: {
            type: DataTypes.STRING,
            validate: {notEmpty: {msg: '-> Missing user'}},
			allowNull: false
		},
		genre: {
			type: DataTypes.STRING,
			allowNull: true
		}
    });
    
    return playlists;
};

module.exports = Playlists;