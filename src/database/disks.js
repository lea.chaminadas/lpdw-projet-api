'use strict';

const Disks = (sequelize, DataTypes) => {
	const disks = sequelize.define('Disks', {
		id: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing id'}}
		},
		title: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing title'}},
			allowNull: false
		},
		totalDuration: {
            type: DataTypes.STRING,
            validate: {notEmpty: {msg: '-> Missing total duration'}},
			allowNull: false
		},
		numberOfTracks: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing number of tracks'}},
			allowNull: false
		},
		genre: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing genre'}},
			allowNull: false
		},
		date: {
			type: DataTypes.DATE,
			validate: {notEmpty: {msg: '-> Missing date'}},
			allowNull: false
		},
		artistId: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing artist'}},
			allowNull: false
		}
    });
    
    return disks;
};

module.exports = Disks;
