'use strict';

const Artists = (sequelize, DataTypes) => {
	return sequelize.define('Artists', {
		id: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing id'}}
		},
		name: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing name'}},
			allowNull: false
		},
		biography: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		label: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing label'}},
			allowNull: false
		},
		genre: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing genre'}},
			allowNull: false
		},
		date: {
			type: DataTypes.DATE,
			validate: {notEmpty: {msg: '-> Missing date'}},
			allowNull: false
		}
	});
};

module.exports = Artists;
