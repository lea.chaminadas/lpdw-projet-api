'use strict';
const path = require('path');
const Sequelize = require('sequelize');

const url = (process.env.DATABASE_URL || '').match(/(.*)\:\/\/(.*?)\:(.*)@(.*)\:(.*)\/(.*)/);
const DB_name     = url ? url[6] : null;
const user        = url ? url[2] : null;
const pwd         = url ? url[3] : null;
const protocol    = url ? url[1] : null;
const dialect     = url ? url[1] : 'sqlite';
const port        = url ? url[5] : null;
const host        = url ? url[4] : null;

// Here we verify the environmental variable to know if we're on test, development or production
const storage     = process.env.NODE_ENV === 'test' ? 'database-test.sqlite' : process.env.DATABASE_STORAGE || 'database.sqlite';

const sequelize = new Sequelize(DB_name, user, pwd, {
	dialect,
	protocol,
	port,
	host,
	storage,
	omitNull: true,
	logging: process.env.NODE_ENV !== 'test'
});

sequelize.sync()
	.then(() => {
		console.log('DB loaded');
	})
;

// TODO: Add here all your mapped models of your database
const Artists = sequelize.import(path.join (__dirname, 'artists'));
const Disks = sequelize.import(path.join(__dirname, 'disks'));
const Songs = sequelize.import(path.join(__dirname, 'songs'));
const Users = sequelize.import(path.join(__dirname, 'users'));
const Playlists = sequelize.import(path.join(__dirname, 'playlists'));

// TODO : Associations

// Artists
Artists.hasMany(Disks, {foreignKey: 'artistId'})
Artists.hasMany(Songs, {foreignKey: 'artistId'})

// Disks
Disks.belongsTo(Artists, {foreignKey: 'artistId', onDelete: 'CASCADE'})
Disks.hasMany(Songs, {foreignKey: 'diskId'})

// Songs
Songs.belongsTo(Artists, {foreignKey: 'artistId'})
Songs.belongsTo(Disks, {foreignKey: 'diskId', onDelete: 'CASCADE'})
Songs.belongsToMany(Playlists, {
	as: 'InPlaylist',
	through: 'playlists_songs',
	foreignKey: 'songId',
	otherKey: 'playlistId'
})

// Users
Users.hasMany(Playlists, {foreignKey: 'userId'})

// Playlists
Playlists.belongsTo(Users, {foreignKey: 'userId'})
Playlists.belongsToMany(Songs, {
	as: 'SongsList',
	through: 'playlists_songs',
	foreignKey: 'playlistId',
	otherKey: 'songId'
})


// TODO: And export them
exports.Artists = Artists;
exports.Disks = Disks;
exports.Songs = Songs;
exports.Users = Users;
exports.Playlists = Playlists;

// Exporting sequelize object to allow raw queries if needed
exports.sequelize = sequelize;
