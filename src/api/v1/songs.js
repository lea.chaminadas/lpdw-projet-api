const express = require('express');
const router = express.Router();


// GET /api/v1/songs - Get a filtered list of songs
router.get('/', async (req, res) => {
	const {title, genre} = req.query;
	const filter = {
		where: {}
	};
	if (title) filter.where.name = title;
	if (genre) filter.where.genre = genre;

	const {Songs} = req.db;
	const songs = await Songs.findAll(filter);

	res.send(songs);
});

// POST /api/v1/songs - Create a song
router.post('/', async (req, res, next) => {
	const {body: newSong} = req;
	try {
		const {Songs} = req.db;
		const song = await Songs.create(newSong);
		res.status(201).send(song);
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeForeignKeyConstraintError') {
			const errMessages = `Artist: ${newSong.artistId} or Disk: ${newSong.diskId} doesn't exist`
			return res.status(400).send({message: errMessages, type: 'invalid-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// GET /api/v1/songs - Geta song via its id
router.get('/:id', async (req, res) => {
	const id = req.params.id

	const {Songs} = req.db;
	const song = await Songs.findOne({ where: {id: id}});

	if (song){
		return res.send(song);
	}else{
		return res.status(404).send({message: 'No song found with the given id: ' + id})
	}

});

// PUT /api/v1/songs - Update a song via its id
router.put('/:id', async (req, res, next) => {
	const id = req.params.id
	const {body: newSongInfos} = req;

	try {
		const {Songs} = req.db;
		const nbRowsUpdated = await Songs.update(newSongInfos, {where: {id: id}});
		if(nbRowsUpdated[0]){
			const songUpdated = await Songs.findOne({where: {id: id}});
			return res.status(200).send(songUpdated)
		}
		return res.status(404).send({message: 'No song found with the given id'});
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeForeignKeyConstraintError') {
			const errMessages = `Artist: ${newSongInfos.artistId} or Disk: ${newSongInfos.diskId} doesn't exist`
			return res.status(400).send({message: errMessages, type: 'invalid-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// DELETE /api/v1/songs - Supprime a song via its id
router.delete('/:id', async (req, res, next) => {
	const id = req.params.id

	const {Songs} = req.db;
	await Songs.destroy({where: {id: id}}).then((rowsDeleted) => {
		if(rowsDeleted){
			return res.status(204).send({message: `Song ${id} was succesfully deleted.`})
		}
		return res.status(404).send({message: `Song ${id} not found`})
	});
});

module.exports = router;