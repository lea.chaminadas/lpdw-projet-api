const express = require('express');
const request = require('request-promise');
const router = express.Router();

// GET /api/v1/users - Get a filtered list of users
router.get('/', async (req, res) => {

	const {Users} = req.db;
	const users = await Users.findAll();

	res.send(users);
});

// POST /api/v1/users - Creates a user
router.post('/', async (req, res, next) => {
	const {body: newUser} = req;
	try {
		const {Users} = req.db;
		const user = await Users.create(newUser);
		res.status(201).send(user);
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// GET /api/v1/users - Get a user via its id
router.get('/:id', async (req, res) => {
	const id = req.params.id

	const {Users} = req.db;
	const user = await Users.findOne({ where: {id: id}});

	if (user){
		return res.send(user);
	}else{
		return res.status(404).send({message: 'No user found with the given id: ' + id})
	}

});

module.exports = router;
