const express = require('express');
const router = express.Router();

// GET /api/v1/disks - Get a filtered list of disks
router.get('/', async (req, res) => {
	const {title, genre} = req.query;
	const filter = {
		where: {}
	};
	if (title) filter.where.name = title;
	if (genre) filter.where.genre = genre;

	const {Disks} = req.db;
	const disks = await Disks.findAll(filter);

	res.send(disks);
});

// POST /api/v1/disks - Create a disk
router.post('/', async (req, res, next) => {
	const {body: newDisk} = req;
	try {
		const {Disks} = req.db;
		const disk = await Disks.create(newDisk);
		res.status(201).send(disk);
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeForeignKeyConstraintError') {
			const errMessages = `${newDisk.artistId} doesn't exist`
			return res.status(400).send({message: errMessages, type: 'invalid-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// GET /api/v1/disks/:id - Gets one disk via its id
router.get('/:id', async (req, res) => {
	const id = req.params.id

	const {Disks} = req.db;
	const disk = await Disks.findOne({ where: {id: id}});

	if (disk){
		return res.send(disk);
	}else{
		return res.status(404).send({message: 'No disk found with the given id: ' + id})
	}

});

// PUT /api/v1/disks/:id - Updates a disk via its id
router.put('/:id', async (req, res, next) => {
	const id = req.params.id
	const {body: newDiskInfos} = req;

	try {
		const {Disks} = req.db;
		const nbRowsUpdated = await Disks.update(newDiskInfos, {where: {id: id}});
		if(nbRowsUpdated[0]){
			const diskUpdated = await Disks.findOne({where: {id: id}});
			return res.status(200).send(diskUpdated)
		}
		return res.status(404).send({message: 'No disk found with the given id'});
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeForeignKeyConstraintError') {
			const errMessages = `${newDiskInfos.artistId} doesn't exist`
			return res.status(400).send({message: errMessages, type: 'invalid-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// DELETE /api/v1/disks - Delete a disk
router.delete('/:id', async (req, res, next) => {
	const id = req.params.id

	const {Disks} = req.db;
	await Disks.destroy({where: {id: id}}).then((rowsDeleted) => {
		if(rowsDeleted){
			return res.status(204).send({message: `Disk ${id} was succesfully deleted.`})
		}
		return res.status(404).send({message: `Disk ${id} not found`})
	});
});

// GET /api/v1/disks - Gets songs of a disk
router.get('/:id/songs', async (req,res,next) => {
	const id = req.params.id;

	const {Disks, Songs} = req.db;

	const disk = await Disks.findOne({ where: {id: id}});

	if (disk){
		try{
			const disksSongs = await Songs.findAll({where: {diskId: id}})
			return res.send(disksSongs);
		}catch(err){
			next(err)
		}
	}else{
		return res.status(404).send({message: 'No artist found with the given id: ' + id})
	}
})

module.exports = router