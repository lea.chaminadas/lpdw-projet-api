const express = require('express');
const createError = require('http-errors');
const router = express.Router();

// GET /api/v1/artists
router.get('/', async (req, res) => {
	const {name, genre, label} = req.query;
	const filter = {
		where: {}
	};
	if (name) filter.where.name = name;
	if (genre) filter.where.genre = genre;
	if (label) filter.where.label = label;

	const {Artists} = req.db;
	const artists = await Artists.findAll(filter);

	res.send(artists);
});

// POST /api/v1/artists
router.post('/', async (req, res, next) => {
	try {
		const {body: newArtist} = req;
		const {Artists} = req.db;
		const artist = await Artists.create(newArtist);
		res.status(201).send(artist);
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// GET /api/v1/artists/:id - Fetch an artist via its id
router.get('/:id', async (req, res) => {
	const id = req.params.id

	const {Artists} = req.db;
	const artist = await Artists.findOne({ where: {id: id}});

	if (artist){
		return res.send(artist);
	}else{
		return res.status(404).send({message: 'No artist found with the given id: ' + id})
	}

});

// PUT /api/v1/artists/:id - Updates an artist via its id
router.put('/:id', async (req, res, next) => {
	const id = req.params.id

	try {
		const {body: newArtistInfos} = req;
		const {Artists} = req.db;
		const nbRowsUpdated = await Artists.update(newArtistInfos, {where: {id: id}});
		if(nbRowsUpdated[0]){
			const artistUpdated = await Artists.findOne({where: {id: id}});
			return res.status(200).send(artistUpdated)
		}
		return res.status(404).send({message: 'No artist found with the given id'});
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// DELETE /api/v1/artists/:id - Delete an artist via its id
router.delete('/:id', async (req, res, next) => {
	const id = req.params.id

	const {Artists} = req.db;
	await Artists.destroy({where: {id: id}}).then((rowsDeleted) => {
		if(rowsDeleted){
			return res.status(204).send({message: `Artist ${id} was succesfully deleted.`})
		}
		return res.status(404).send({message: `Artist ${id} not found`})
	});
});

// GET /api/v1/artists/:id/disks - Get all disks of an artist
router.get('/:id/disks', async (req,res,next) => {
	const id = req.params.id;

	const {Artists} = req.db;
	const {Disks} = req.db;

	const artist = await Artists.findOne({ where: {id: id}});

	if (artist){
		try{
			const artistsDisks = await Disks.findAll({
				include:[{
					model: Artists,
					where: {id: id}
				}]
			  })
			  return res.send(artistsDisks);
		}catch(err){
			next(err)
		}
	}else{
		return res.status(404).send({message: 'No artist found with the given id: ' + id})
	}
})

// GET /api/v1/artists/:id/songs - Get all songs of an artist
router.get('/:id/songs', async (req,res,next) => {
	const id = req.params.id;

	const {Artists} = req.db;
	const {Songs} = req.db;

	const artist = await Artists.findOne({ where: {id: id}});

	if (artist){
		try{
			const artistsSongs = await Songs.findAll({
				include:[{
					model: Artists,
					where: {id: id}
				}]
			  })
			  return res.send(artistsSongs);
		}catch(err){
			next(err)
		}
	}else{
		return res.status(404).send({message: 'No artist found with the given id: ' + id})
	}
})


module.exports = router;