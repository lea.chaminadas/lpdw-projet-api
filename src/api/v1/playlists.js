const express = require('express');
const router = express.Router();

// GET /api/v1/playlists - Gets a filtered list of playlists
router.get('/', async (req, res) => {
	const {title, genre} = req.query;
	const filter = {
		where: {}
	};
	if (title) filter.where.name = title;
	if (genre) filter.where.genre = genre;

	const {Playlists} = req.db;
	const playlists = await Playlists.findAll(filter);

	res.send(playlists);
});

// POST /api/v1/playlists - Creates a playlist
router.post('/', async (req, res, next) => {
	const {body: newPlaylist} = req;

	try {
		const {Playlists} = req.db;
		const playlist = await Playlists.create(newPlaylist);
		res.status(201).send(playlist);
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeForeignKeyConstraintError') {
			const errMessages = `${newPlaylist.userId} doesn't exist`
			return res.status(400).send({message: errMessages, type: 'invalid-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});


// GET /api/v1/playlists/:id - Gets a playlist via its id
router.get('/:id', async (req, res) => {
	const id = req.params.id

	const {Playlists} = req.db;
	const playlist = await Playlists.findOne({ where: {id: id}});

	if (playlist){
		return res.send(playlist);
	}else{
		return res.status(404).send({message: 'No playlist found with the given id: ' + id})
	}

});

// PUT /api/v1/playlists/:id - Updates a playlist via its id
router.put('/:id', async (req, res, next) => {
	const id = req.params.id
	const {body: newPlaylistInfos} = req;

	try {
		const {Playlists} = req.db;
		const nbRowsUpdated = await Playlists.update(newPlaylistInfos, {where: {id: id}});
		if(nbRowsUpdated[0]){
			const playlistUpdated = await Playlists.findOne({where: {id: id}});
			return res.status(200).send(playlistUpdated)
		}
		return res.status(404).send({message: 'No playlist found with the given id'});
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			const errMessages = err.errors.map(err => `${err.path} cannot be ${err.validatorName}`)
			return res.status(400).send({message: errMessages, type: 'missing-data'});
		}
		if (err.name === 'SequelizeForeignKeyConstraintError') {
			const errMessages = `${newPlaylistInfos.artistId} doesn't exist`
			return res.status(400).send({message: errMessages, type: 'invalid-data'});
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			const errMessages = err.errors.map(err => err.message)
			return res.status(400).send({message: errMessages, type: 'existing-data'});
		}
		next(err);
	}
});

// DELETE /api/v1/playlists/:id - Deletes a playlist via its id
router.delete('/:id', async (req, res, next) => {
	const id = req.params.id

	const {Playlists} = req.db;
	await Playlists.destroy({where: {id: id}}).then((rowsDeleted) => {
		if(rowsDeleted){
			return res.status(204).send({message: `Playlist ${id} was succesfully deleted.`})
		}
		return res.status(404).send({message: `Playlist ${id} not found`})
	});
});

// GET /api/v1/playlists/:id/songs - Gets playlist's songs
router.get('/:id/songs', async (req,res,next) => {
	const id = req.params.id;

	const {Playlists} = req.db;

	const playlist = await Playlists.findOne({ where: {id: id}});

	if (playlist){
		try{
			const playlistsSongs = await playlist.getSongsList()
			return res.send(playlistsSongs);
		}catch(err){
			next(err)
		}
	}else{
		return res.status(404).send({message: 'No artist found with the given id: ' + id})
	}
})

// POST /api/v1/playlists/:id/songs/:songId - Add a song to a playlist
router.post('/:id/songs/:songId', async (req,res,next) => {
    const {id, songId} = req.params;

	const {Playlists} = req.db;
	const {Songs} = req.db;

    const playlist = await Playlists.findOne({ where: {id: id}});
    const song = await Songs.findOne({ where: {id: songId}});
    
    if (!playlist){
        return res.status(404).send({message: 'No playlist found with the given id: ' + id})
    }

    if (!song){
        return res.status(400).send({message: 'No song found with the given id: ' + songId})
    }

	playlist.addSongsList(song)
		.then((songs) =>{
			res.send({message: `Song ${songId} added successfully to the playlist ${id}`})
		})
		.catch((err)=>{
			next(err)
		});
})

// DELETE /api/v1/playlists/:id/songs/:songId - Removes a song from a playlist
router.delete('/:id/songs/:songId', async (req,res,next) => {
    const {id, songId} = req.params;

	const {Playlists} = req.db;
	const {Songs} = req.db;

    const playlist = await Playlists.findOne({ where: {id: id}});
    const song = await Songs.findOne({ where: {id: songId}});
    
    if (!playlist){
        return res.status(404).send({message: 'No playlist found with the given id: ' + id})
    }

    if (!song){
        return res.status(400).send({message: 'No song found with the given id: ' + songId})
    }

	playlist.removeSongsList([song])
		.then((songs) =>{
			res.sendStatus(204)
		})
		.catch((err)=>{
			next(err)
		});
})

module.exports = router