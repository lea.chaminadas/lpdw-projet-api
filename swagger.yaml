swagger: "2.0"
info:
  description: "API template"
  version: "0.0.0"
  title: "API template"
host: "localhost:3000"
basePath: "/api/v1"
paths:
  /users:
    get:
      tags:
        - users
      summary: Retrieves a list of users
      parameters:
        - name: username
          type: string
          in: query
          description: Username of the users
        - name: fullname
          type: string
          in: query
          description: Fullname of the users
      responses:
        200:
          description: List of filtered users according to the restrictions given via querystring
          schema:
            type: array
            items:
              $ref: "#/definitions/User"
    post:
      tags:
        - users
      summary: Creates a user
      parameters:
        - name: body
          in: body
          schema:
            $ref: "#/definitions/User"
          description: User informations
      responses:
        201:
          description: Created user
          schema:
            $ref: "#/definitions/User"
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
  /users/{id}:
    get:
      tags:
        - users
      summary: Returns a User
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: The given user
          schema:
            $ref: '#/definitions/User'
        404:
          description: This id is not coupled with any user
          schema:
            $ref: "#/definitions/Error"
  /playlists:
    get:
      tags:
        - playlists
      summary: Returns a list of playlists
      parameters:
        - name: title
          type: string
          in: query
          description: Title of the playlist
        - name: genre
          type: string
          in: query
          description: Genre of the playlist
      responses:
        200:
          description: List of filtered playlists according to the restrictions given via querystring
          schema:
            type: array
            items:
              $ref: "#/definitions/Playlist"
    post:
      tags:
        - playlists
      summary: Creates a playlist
      parameters:
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Playlist"
          description: Playlist informations
      responses:
        201:
          description: Created playlist
          schema:
            $ref: "#/definitions/Playlist"
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
  /playlists/{id}:
    get:
      tags:
        - playlists
      summary: Gets a playlist via its id
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: The given playlist
          schema:
            $ref: '#/definitions/Playlist'
        404:
          description: This id is not coupled with any playlist
          schema:
            $ref: "#/definitions/Error"
    put:
      tags:
        - playlists
      summary: Updates a playlist via its id
      parameters:
        - in: path
          name: id
          required: true
          type: string
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Playlist"
          description: Playlist informations
      responses:
        200:
          description: The given playlist updated
          schema:
            $ref: '#/definitions/Playlist'
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
        404:
          description: This id is not coupled with any playlist
          schema:
            $ref: "#/definitions/Error"
    delete:
      tags:
        - playlists
      summary: Deletes a playlist via its id
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        204:
          description: The given playlist was deleted
          schema:
            $ref: '#/definitions/Playlist'
        404:
          description: This id is not coupled with any playlist
          schema:
            $ref: "#/definitions/Error"
  /playlists/{id}/songs:
    get:
      tags:
        - playlists
      summary: Gets a playlist's songs
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: The given song was added to the playlist
          schema:
            type: array
            items:
              $ref: "#/definitions/Song"
  /playlists/{id}/songs/{songId}:
    post:
      tags:
        - playlists
      summary: Gets a playlist's songs
      parameters:
        - in: path
          name: id
          required: true
          type: string
        - in: path
          name: songId
          required: true
          type: string
      responses:
        201:
          description: The given song was added to the playlist
          schema:
            $ref: '#/definitions/Message'
        400:
          description: The song doesn't exist
          schema:
            $ref: "#/definitions/Error"
        404:
          description: The playlist was not found
          schema:
            $ref: "#/definitions/Error"
    delete:
      tags:
        - playlists
      summary: Removes a playlist's songs
      parameters:
        - in: path
          name: id
          required: true
          type: string
        - in: path
          name: songId
          required: true
          type: string
      responses:
        204:
          description: The given song was removed from the playlist
        400:
          description: The song doesn't exist
          schema:
            $ref: "#/definitions/Error"
        404:
          description: The playlist was not found
          schema:
            $ref: "#/definitions/Error"
  /songs:
    get:
      tags:
        - songs
      summary: Returns a list of songs
      parameters:
        - name: title
          type: string
          in: query
          description: Title of song
        - name: genre
          type: string
          in: query
          description: Genre of song
      responses:
        200:
          description: List of filtered songs according to the restrictions given via querystring
          schema:
            type: array
            items:
              $ref: "#/definitions/Song"
    post:
      tags:
        - songs
      summary: Creates a song
      parameters:
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Song"
          description: Song informations
      responses:
        201:
          description: Created song
          schema:
            $ref: "#/definitions/Song"
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
  /songs/{id}:
    get:
      tags:
        - songs
      summary: Returns a Song
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: The given song
          schema:
            $ref: '#/definitions/Song'
        404:
          description: This id is not coupled with any song
          schema:
            $ref: "#/definitions/Error"
    put:
      tags:
        - songs
      summary: Update a song
      parameters:
        - in: path
          name: id
          required: true
          type: string
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Song"
          description: Song informations
      responses:
        200:
          description: The given song updated
          schema:
            $ref: '#/definitions/Song'
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
        404:
          description: This id is not coupled with any song
          schema:
            $ref: "#/definitions/Error"
    delete:
      tags:
        - songs
      summary: Delete a song
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        204:
          description: The given song was deleted
          schema:
            $ref: '#/definitions/Song'
        404:
          description: This id is not coupled with any song
          schema:
            $ref: "#/definitions/Error"
  /disks:
    get:
      tags:
        - disks
      summary: Returns a list of disks
      parameters:
        - name: title
          type: string
          in: query
          description: Title of disk
        - name: genre
          type: string
          in: query
          description: Genre of disk
      responses:
        200:
          description: List of filtered disks according to the restrictions given via querystring
          schema:
            type: array
            items:
              $ref: "#/definitions/Disk"
    post:
      tags:
        - disks
      summary: Creates a disk
      parameters:
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Disk"
          description: Disk informations
      responses:
        201:
          description: Created disk
          schema:
            $ref: "#/definitions/Disk"
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
  /disks/{id}:
    get:
      tags:
        - disks
      summary: Returns a Disk
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: The given disk
          schema:
            $ref: '#/definitions/Disk'
        404:
          description: This id is not coupled with any disk
          schema:
            $ref: "#/definitions/Error"
    put:
      tags:
        - disks
      summary: Update a disk
      parameters:
        - in: path
          name: id
          required: true
          type: string
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Disk"
          description: Disk informations
      responses:
        200:
          description: The given disk updated
          schema:
            $ref: '#/definitions/Disk'
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
        404:
          description: This id is not coupled with any disk
          schema:
            $ref: "#/definitions/Error"
    delete:
      tags:
        - disks
      summary: Delete a disk
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        204:
          description: The given disk was deleted
          schema:
            $ref: '#/definitions/Disk'
        404:
          description: This id is not coupled with any disk
          schema:
            $ref: "#/definitions/Error"
  /artists:
    get:
      tags:
        - artists
      summary: Returns a list of artists
      parameters:
        - name: name
          type: string
          in: query
          description: Name of the artist
        - name: genre
          type: string
          in: query
          description: Genre of the artist
        - name: label
          type: string
          in: query
          description: Label of the artist
      responses:
        200:
          description: List of filtered artists according to the restrictions given via querystring
          schema:
            type: array
            items:
              $ref: "#/definitions/Artist"
    post:
      tags:
        - artists
      summary: Creates an artist
      parameters:
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Artist"
          description: Artist informations
      responses:
        201:
          description: Created artist
          schema:
            $ref: "#/definitions/Artist"
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
  /artists/{id}:
    get:
      tags:
        - artists
      summary: Get an artist via its id
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: The given artist
          schema:
            $ref: '#/definitions/Artist'
        404:
          description: This id is not coupled with any artist
          schema:
            $ref: "#/definitions/Error"
    put:
      tags:
        - artists
      summary: Updates an artist via its id
      parameters:
        - in: path
          name: id
          required: true
          type: string
        - name: body
          in: body
          schema:
            $ref: "#/definitions/Artist"
          description: Artist informations
      responses:
        200:
          description: The given artist updated
          schema:
            $ref: '#/definitions/Artist'
        400:
          description: Missing mandatory data or violating unique constraint
          schema:
            $ref: "#/definitions/Error"
        404:
          description: This id is not coupled with any artist
          schema:
            $ref: "#/definitions/Error"
    delete:
      tags:
        - artists
      summary: Delete an artist via its id
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        204:
          description: The given artist was deleted
          schema:
            $ref: '#/definitions/Artist'
        404:
          description: This id is not coupled with any artist
          schema:
            $ref: "#/definitions/Error"
  /artists/{id}/disks:
    get:
      tags:
        - artists
      summary: Gets an artist's disks
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: Artist's disks retrieved successfully
          schema:
            type: array
            items:
              $ref: "#/definitions/Disk"
        404:
          description: This id is not coupled with any artist
          schema:
            $ref: "#/definitions/Error"
  /artists/{id}/songs:
    get:
      tags:
        - artists
      summary: Gets an artist's songs
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: Artist's songs retrieved successfully
          schema:
            type: array
            items:
              $ref: "#/definitions/Song"
        404:
          description: This id is not coupled with any artist
          schema:
            $ref: "#/definitions/Error"
definitions:
  User:
    type: object
    properties:
      id:
        type: string
        description: id of the user
      username:
        type: string
        description: username of the user
      fullname:
        type: string
        description: fullname of the user
      email:
        type: string
        description: email of the user
      hashPassword:
        type: string
        description: hashPassword of the user
    example:
      id: "USR-123"
      username: "@devonbl"
      fullname: "Devon Lindsey"
      email: "devon@yahoo.com"
      hashPassword: "hgtF@.?rgz"
  Playlist:
    type: object
    properties:
      id:
        type: string
        description: id of the playlist
      title:
        type: string
        description: title of the playlist
      userId:
        type: string
        description: id of the user
      genre:
        type: string
        description: genre of the playlist
    example:
      id: "PLA-123"
      title: "Morning"
      userId: "USR-123"
      genre: "Pop"
  Song:
    type: object
    properties:
      id:
        type: string
        description: id of the song
      title:
        type: string
        description: title of the song
      diskId:
        type: string
        description: id of the disk
      artistId:
        type: string
        description: id of the artist
      duration:
        type: integer
        description: duration of the song
      genre:
        type: string
        description: genre of the song
    example:
      id: "SON-123"
      title: "Hello"
      diskId: "DIS-123"
      artistId: "ART-123"
      duration: "00:03:00"
      genre: "Pop"
  Artist:
    type: object
    properties:
      id:
        type: string
        description: Identifier of the artist
      name:
        type: string
        description: Name of the artist
      biography:
        type: string
        description: Biography of the artist
      label:
        type: string
        description: Label of the artist
      date:
        type: string
        format: date
        description: When the artist debuted
      genre:
        type: string
        description: Principal genre of the artist
    example:
      id: 'ART-123'
      name: 'Adele'
      biography: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin iaculis a nisl eget rutrum. Phasellus id elit euismod, iaculis ex eu, molestie dolor. Cras vestibulum non sapien sit amet vehicula. Integer at lacinia diam, quis auctor augue. Pellentesque sodales interdum nulla sed convallis. Nulla venenatis ex eget odio fermentum ultricies. Nullam eu fermentum orci. Vestibulum hendrerit placerat nisi, nec luctus neque malesuada eu. Praesent ornare, odio eget mattis eleifend, velit odio pretium orci, non dapibus felis odio sit amet diam.'
      date: '2018-03-06'
      label: 'Sony Musics'
      genre: 'Pop' 
  Disk:
    type: object
    properties:
      id:
        type: string
        description: Identifier of the disk
      title:
        type: string
        description: title of the disk
      numberOfTracks:
        type: integer
        description: Number of tracks of the disk
      totalDuration:
        type: string
        description: Duration od the disk
      date:
        type: string
        format: date
        description: When the disk was launched
      genre:
        type: string
        description: Principal genre of the disk
      artistId:
        type: string
        description: Id of the artist
    example:
      id: 'DIS-123'
      title: '25'
      artistId: 'ART-123'
      date: '2018-03-06'
      numberOfTracks: 15
      totalDuration: '00:50:00'
      genre: 'Pop'
  Error:
    type: object
    properties:
      message:
        type: array
      type:
        type: string
    example:
      message: 
        - Description error 1
        - Description error 2
      type: 'missing-data'
  Message:
    type: object
    properties:
      message:
        type: string
    example:
      message: 'Success'
