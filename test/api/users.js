'use strict';

const {
    expect
} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixturesUsers = require('../fixtures/users');

const server = request(createServer());

describe('User api', function () {
    before(async function () {
        await database.sequelize.query('DELETE from USERS');

        const {
            Users
        } = database;

        const promisesUsers = fixturesUsers.map(user => Users.create(user));
        await Promise.all(promisesUsers);
    });

    describe('GET /api/v1/users', function () {
        it('Recover all users', async () => {
            const {
                body: users
            } = await server
                .get('/api/v1/users')
                .set('Accept', 'application/json')
                .expect(200);
            expect(users).to.be.an('array');
            expect(users.length).to.equal(2);
        });
    });

    describe('POST /api/v1/users', function () {
        it('should create an user', async () => {
            const {
                body: user
            } = await server
                .post('/api/v1/users')
                .set('Accept', 'application/json')
                .send({
                    id: "USER-789",
                    username: "xXUserxx",
                    fullname: "John Doe",
                    email: "john@doe.fr",
                    hashPassword: "slbdfs846sdfsdf4s64fe6fsdfsdq98741",
                    public: true
                })
                .expect(201);
        });
        it('should not create an user and send a bad request error because ID is already used', async () => {
            const {
                body: error
            } = await server
                .post('/api/v1/users')
                .set('Accept', 'application/json')
                .send({
                    id: "USER-789",
                    username: "xXUserxx",
                    fullname: "John Doe",
                    email: "john@doe.fr",
                    hashPassword: "slbdfs846sdfsdf4s64fe6fsdfsdq98741",
                    public: true
                })
                .expect(400);
            expect(error.type).to.equal('existing-data');
        });
        it('should not create an user and send a bad request error if data are missing', async () => {
            const {
                body: error
            } = await server
                .post('/api/v1/users')
                .set('Accept', 'application/json')
                .send({
                    id: "USER-789",
                    username: "xXUserxx",
                    fullname: "John Doe",
                    hashPassword: "slbdfs846sdfsdf4s64fe6fsdfsdq98741",
                    public: true
                })
                .expect(400);
            expect(error.type).to.equal('missing-data');
        });
        it('should not create an user and send a bad request error username is already used', async () => {
            const {
                body: error
            } = await server
                .post('/api/v1/users')
                .set('Accept', 'application/json')
                .send({
                    id: "USER-101112",
                    username: "xXUserxx",
                    fullname: "John Doe",
                    email: "john+test@doe.fr",
                    hashPassword: "slbdfs846sdfsdf4s64fe6fsdfsdq98741",
                    public: true
                })
                .expect(400);
            expect(error.type).to.equal('existing-data');
        });
    });
    describe('GET /api/v1/users/:id', function () {
        it('Recover one user by its id', async () => {
            const {
                body: user
            } = await server
                .get('/api/v1/users/USER-123')
                .set('Accept', 'application/json')
                .expect(200)
            expect(user.fullname).to.equal("Léa Chaminadas")
        });
        it('should not recover any user', async () => {
            await server
                .get('/api/v1/users/USER-121')
                .set('Accept', 'application/json')
                .expect(404)
        });
    });
});