'use strict';

const { expect } = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixturesDisks = require('../fixtures/disks')
const fixturesArtists = require('../fixtures/artists')
const fixturesSongs = require('../fixtures/songs')

const server = request(createServer());

describe('Disk api', function () {
	before(async function () {
		await database.sequelize.query('DELETE from Artists');
        await database.sequelize.query('DELETE from Disks');
        await database.sequelize.query('DELETE from Songs');
        
		const { Disks } = database;
        const { Artists } = database;
        const { Songs } = database;
        
        const promisesArtists = fixturesArtists.map(artist => Artists.create(artist));
        await Promise.all(promisesArtists)
        
		const promisesDisks = fixturesDisks.map(disk => Disks.create(disk));
		await Promise.all(promisesDisks);
        
        const promisesSongs = fixturesSongs.map(song => Songs.create(song));
		await Promise.all(promisesSongs);
	});

	describe('GET /api/v1/songs', function () {
		it('Recover all songs', async () => {
			const { body: songs } = await server
				.get('/api/v1/songs')
				.set('Accept', 'application/json')
				.expect(200);
			expect(songs).to.be.an('array');
			expect(songs.length).to.equal(2);
		});
		it('Filtering songs', async () => {
			const { body: songs } = await server
				.get('/api/v1/songs')
				.query({
					genre: 'Pop'
				})
				.set('Accept', 'application/json')
				.expect(200);

			expect(songs).to.be.an('array');
			expect(songs.length).to.equal(1);
			expect(songs[0].id).to.equal('SON-123');
		});
	});

	describe('POST /api/v1/songs', function () {
		it('should create an song', async () => {
            const {body: error} = await server
				.post('/api/v1/songs')
				.set('Accept', 'application/json')
				.send({
					id: "SON-789",
                    title: "Undisclosed Desires",
                    artistId: "ART-456",
                    diskId: "DIS-456",
                    duration: "04:21",
                    genre: "Alternative"
				})
				.expect(201);
        });
		it('should not create an song and send a bad request error because ID is already used', async () => {
            const {body: error} = await server
				.post('/api/v1/songs')
				.set('Accept', 'application/json')
				.send({
					id: "SON-456",
                    title: "Undisclosed Disires",
                    artistId: "ART-456",
                    diskId: "DIS-456",
                    duration: "04:21",
                    genre: "Alternative"
				})
				.expect(400);
			expect(error.type).to.equal('existing-data');
		});
		it('should not create an song and send a bad request error if data are missing', async () => {
			const {body: error} = await server
				.post('/api/v1/songs')
				.set('Accept', 'application/json')
				.send({
					id: "SON-456",
					title: "Undisclosed Disires",
					artistId: "ART-456",
					diskId: "DIS-456",
					genre: "Alternative"
				})
				.expect(400);
			expect(error.type).to.equal('missing-data');
		});
		it('should not create an song and send a bad request error if linked artist does not exist', async () => {
			const {body: error} = await server
				.post('/api/v1/songs')
				.set('Accept', 'application/json')
				.send({
					id: "SON-101112",
					title: "Undisclosed Disires",
					artistId: "ART-121",
					diskId: "DIS-121",
					duration: "04:21",
					genre: "Alternative"
				})
				.expect(400);
			expect(error.type).to.equal('invalid-data');
        });
	});
	describe('GET /api/v1/songs/:id', function () {
		it('Recover one song by its id', async () => {
			const {body: song} = await server
				.get('/api/v1/songs/SON-123')
				.set('Accept', 'application/json')
				.expect(200)
			expect(song.genre).to.equal("Pop")
		});
		it('should not recover any song', async () => {
			await server
				.get('/api/v1/songs/SON-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
	describe('PUT /api/v1/songs/:id', function () {
		it('should update an song', async () => {
			const { body: disk } = await server
				.put('/api/v1/songs/SON-123')
				.set('Accept', 'application/json')
				.send({
					genre: 'Punk'
				})
				.expect(200)
			expect(disk.genre, 'Punk');
		});
		it('should not found the song', async () => {
			await server
				.put('/api/v1/songs/SON-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
		it('should not update an song and send a bad request error because ID is already used', async () => {
            const {body: error} = await server
				.put('/api/v1/songs/SON-123')
				.set('Accept', 'application/json')
				.send({
					id: "SON-456"
				})
				.expect(400);
			expect(error.type).to.equal('existing-data');
		});
		it('should not update an song and send a bad request error if data are missing', async () => {
			const {body: error} = await server
				.put('/api/v1/songs/SON-123')
				.set('Accept', 'application/json')
				.send({
					genre: ""
				})
				.expect(400);
			expect(error.type).to.equal('missing-data');
		});
		it('should not create an song and send a bad request error if linked artist does not exist', async () => {
			const {body: error} = await server
				.put('/api/v1/songs/SON-123')
				.set('Accept', 'application/json')
				.send({
					artistId: "ART-121",
					diskId: "DIS-121",
				})
				.expect(400);
			expect(error.type).to.equal('invalid-data');
        });
	});
	describe('DELETE /api/v1/songs/:id', function () {
		it('should delete an song', async () => {
			const response = await server
				.delete('/api/v1/songs/SON-123')
				.expect(204)
			expect(response, undefined);
		});
		it('should not found the song', async () => {
			const response = await server
				.delete('/api/v1/songs/SON-121')
				.expect(404)
		});
	});
});