'use strict';

const { expect } = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixturesSongs = require('../fixtures/songs')
const fixturesPlaylists = require('../fixtures/playlists')
const fixturesUsers = require('../fixtures/users')
const fixturesDisks = require('../fixtures/disks')
const fixturesArtists = require('../fixtures/artists')

const server = request(createServer());

describe('Playlist api', function () {
	before(async function () {

		await database.sequelize.query('DELETE from ARTISTS');
        await database.sequelize.query('DELETE from DISKS');
        await database.sequelize.query('DELETE from SONGS');
        await database.sequelize.query('DELETE from USERS');
		await database.sequelize.query('DELETE from PLAYLISTS');
        
        const { Artists } = database;
        const { Disks } = database;
		const { Songs } = database;
        const { Users } = database;
        const { Playlists } = database;

        const promisesArtists = fixturesArtists.map(artist => Artists.create(artist));
        await Promise.all(promisesArtists)
        
		const promisesDisks = fixturesDisks.map(disk => Disks.create(disk));
		await Promise.all(promisesDisks);
        
        const promisesUsers = fixturesUsers.map(user => Users.create(user));
        await Promise.all(promisesUsers)
        
        const promisesSongs = fixturesSongs.map(song => Songs.create(song));
		await Promise.all(promisesSongs);

        const promisesPlaylists = fixturesPlaylists.map(playlist => Playlists.create(playlist));
        await Promise.all(promisesPlaylists)

	});

	describe('GET /api/v1/playlists', function () {
		it('Recover all playlists', async () => {
			const { body: playlists } = await server
				.get('/api/v1/playlists')
				.set('Accept', 'application/json')
				.expect(200);
			expect(playlists).to.be.an('array');
			expect(playlists.length).to.equal(2);
		});
		it('Filtering playlists', async () => {
			const { body: playlists } = await server
				.get('/api/v1/playlists')
				.query({
					genre: 'Pop'
				})
				.set('Accept', 'application/json')
				.expect(200);

			expect(playlists).to.be.an('array');
			expect(playlists.length).to.equal(1);
			expect(playlists[0].id).to.equal('PLA-123');
		});
	});

	describe('POST /api/v1/playlists', function () {
		it('should create an playlist', async () => {
			const { body: playlist } = await server
				.post('/api/v1/playlists')
				.set('Accept', 'application/json')
				.send({
					id: 'PLA-789',
					title: 'Tuesday Morning',
					userId: 'USER-123',
					genre: 'Pop'
				})
				.expect(201);

			expect(playlist).to.be.an('object');
			expect(playlist.id).to.equal('PLA-789');
			expect(playlist.userId).to.equal('USER-123');
		});
		it('should not create an playlist and send a bad request error because ID is already used', async () => {
			const {body: error} = await server
				.post('/api/v1/playlists')
				.set('Accept', 'application/json')
				.send({
					id: 'PLA-456',
					title: 'Tuesday Morning',
					userId: 'USER-123',
					genre: 'Pop'
				})
				.expect(400);
				expect(error.type).to.equal('existing-data');
		});
		it('should not create an artist and send a bad request error if data are missing', async () => {
			const { body: error } = await server
			.post('/api/v1/playlists')
			.set('Accept', 'application/json')
			.send({
				id: 'PLA-101112',
				userId: 'USER-123'
			})
			.expect(400);
			expect(error.type).to.equal('missing-data');
		});
		it('should not create an artist and send a bad request error if linked artist does not exist', async () => {
			const { body: error } = await server
			.post('/api/v1/playlists')
			.set('Accept', 'application/json')
			.send({
				id: 'PLA-131415',
				title: 'Tuesday Morning',
				userId: 'USER-121',
				genre: 'Pop'
			})
			.expect(400);
		expect(error.type).to.equal('invalid-data');
		});
	});
	describe('GET /api/v1/playlists/:id', function () {
		it('Recover one playlist by its id', async () => {
			const { body: playlist } = await server
				.get('/api/v1/playlists/PLA-123')
				.set('Accept', 'application/json')
				.expect(200)
			expect(playlist.genre, 'Pop');
		});
		it('should not recover any playlist', async () => {
			await server
				.get('/api/v1/playlists/PLA-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
	describe('PUT /api/v1/playlists/:id', function () {
		it('should update an playlist', async () => {
			const { body: playlist } = await server
				.put('/api/v1/playlists/PLA-123')
				.set('Accept', 'application/json')
				.send({
					genre: 'Pop/Rock'
				})
				.expect(200)
			expect(playlist.genre, 'Pop/Rock');
		});
		it('should not found the playlist', async () => {
			await server
				.put('/api/v1/playlists/PLA-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
		it('should not update an artist and send a bad request error if data are missing', async () => {
			const { body: error } = await server
				.put('/api/v1/playlists/PLA-123')
				.set('Accept', 'application/json')
				.send({
					title: ''
				})
				.expect(400);
			expect(error.type).to.equal('missing-data');
		});
		it('should not update an artist and send a bad request error if linked artist does not exist', async () => {
			const { body: error } = await server
				.put('/api/v1/playlists/PLA-123')
				.set('Accept', 'application/json')
				.send({
					userId: 'USER-121'
				})
			.expect(400);
			expect(error.type).to.equal('invalid-data');
		});
	});
	describe('DELETE /api/v1/playlists/:id', function () {
		it('should delete an playlist', async () => {
			const response = await server
				.delete('/api/v1/playlists/PLA-123')
				.expect(204)
			expect(response, undefined);
		});
		it('should not found the playlist', async () => {
			await server
				.put('/api/v1/playlists/PLA-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
    });
    describe('POST /api/v1/playlists/:id/songs/:songId', function () {
        it('Recover one artist\'s songs', async () => {
            const { body: success } = await server
				.post('/api/v1/playlists/PLA-456/songs/SON-123')
				.set('Accept', 'application/json')
				.expect(200)
            
			expect(success.message).to.equal("Song SON-123 added successfully to the playlist PLA-456")
			
		});
		it('should not add any songs to this playlist', async () => {
            await server
				.post('/api/v1/playlists/PLA-121/songs/SON-123')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
    describe('GET /api/v1/playlists/:id/songs', function () {
        it('Recover one playlist\'s songs', async () => {
            const { body: songs } = await server
                .get('/api/v1/playlists/PLA-456/songs')
                .set('Accept', 'application/json')
                .expect(200)
            
            expect(songs.length).to.equal(1)
            
        });
        it('should not recover any playlist', async () => {
            await server
                .get('/api/v1/playlists/PLA-121/songs')
                .set('Accept', 'application/json')
                .expect(404)
        });
	});
	describe('DELETE /api/v1/playlists/:id/songs/:songId', function () {
        it('Recover one artist\'s songs', async () => {
            await server
            .delete('/api/v1/playlists/PLA-456/songs/SON-123')
            .set('Accept', 'application/json')
            .expect(204)
		});
		it('should not add any songs to this playlist', async () => {
            await server
            .delete('/api/v1/playlists/PLA-121/songs')
            .set('Accept', 'application/json')
            .expect(404)
		});
	});
});