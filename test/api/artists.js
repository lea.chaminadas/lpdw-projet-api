'use strict';

const { expect } = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixturesArtists = require('../fixtures/artists')
const fixturesDisks = require('../fixtures/disks')
const fixturesSongs = require('../fixtures/songs')


const server = request(createServer());

describe('Artist api', function () {
	before(async function () {
		await database.sequelize.query('DELETE from Artists');
        await database.sequelize.query('DELETE from Disks');
        await database.sequelize.query('DELETE from Songs');
        
		const { Disks } = database;
        const { Artists } = database;
        const { Songs } = database;
        
        const promisesArtists = fixturesArtists.map(artist => Artists.create(artist));
        await Promise.all(promisesArtists)
        
		const promisesDisks = fixturesDisks.map(disk => Disks.create(disk));
		await Promise.all(promisesDisks);
        
        const promisesSongs = fixturesSongs.map(song => Songs.create(song));
		await Promise.all(promisesSongs);
	});

	describe('GET /api/v1/artists', function () {
		it('Recover all artists', async () => {
			const { body: artists } = await server
				.get('/api/v1/artists')
				.set('Accept', 'application/json')
				.expect(200);
			expect(artists).to.be.an('array');
			expect(artists.length).to.equal(3);
		});
		it('Filtering artists', async () => {
			const { body: artists } = await server
				.get('/api/v1/artists')
				.query({
					label: 'Sony Musics',
					name: 'Adele',
					genre: 'Pop'
				})
				.set('Accept', 'application/json')
				.expect(200);

			expect(artists).to.be.an('array');
			expect(artists.length).to.equal(1);
			expect(artists[0].id).to.equal('ART-123');
		});
	});

	describe('POST /api/v1/artists', function () {
		it('should create an artist', async () => {
			const { body: artist } = await server
				.post('/api/v1/artists')
				.set('Accept', 'application/json')
				.send({
					id: 'ART-101112',
					name: 'Mickael Jackson',
					label: 'Mowtown',
					date: '2019-01-01',
					biography: 'King of Pop',
					genre: 'Pop'
				})
				.expect(201);

			expect(artist).to.be.an('object');
			expect(artist.id).to.equal('ART-101112');
		});
		it('should not create an artist and send a bad request error if not all mandatory data are not given', async () => {
			const { body: artist } = await server
				.post('/api/v1/artists')
				.set('Accept', 'application/json')
				.send({
					id: 'ART-101112',
					name: 'Mickael Jackson',
					label: 'Mowtown',
					date: '2019-01-01',
					biography: 'King of Pop'
				})
				.expect(400);
		});
		it('should not create an artist and send a bad request error if id is not unique', async () => {
			const { body: artist } = await server
				.post('/api/v1/artists')
				.set('Accept', 'application/json')
				.send({
					id: 'ART-123',
					name: 'Mickael Jackson',
					label: 'Mowtown',
					date: '2019-01-01',
					biography: 'King of Pop',
					genre: 'Pop'
				})
				.expect(400);
		});
	});
	describe('GET /api/v1/artists/:id', function () {
		it('Recover one artist by its id', async () => {
			const { body: artist } = await server
				.get('/api/v1/artists/ART-123')
				.set('Accept', 'application/json')
				.expect(200)
			expect(artist.label, 'Sony Musics');
		});
		it('should not recover any artist', async () => {
			await server
				.get('/api/v1/artists/ART-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
	describe('PUT /api/v1/artists/:id', function () {
		it('should update an artist', async () => {
			const { body: artist } = await server
				.put('/api/v1/artists/ART-123')
				.set('Accept', 'application/json')
				.send({
					label: 'Sony'
				})
				.expect(200)
			expect(artist.label, 'Sony');
		});
		it('should not found the artist', async () => {
			await server
				.put('/api/v1/artists/ART-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
		it('should not create an artist and send a bad request error if not all mandatory data are not given', async () => {
			const { body: error } = await server
				.put('/api/v1/artists/ART-123')
				.set('Accept', 'application/json')
				.send({
					genre: ''
				})
				.expect(400);
			expect(error.type).to.equal('missing-data');
		});
		it('should not create an artist and send a bad request error if id is not unique', async () => {
			const { body: error } = await server
				.put('/api/v1/artists/ART-123')
				.set('Accept', 'application/json')
				.send({
					id: 'ART-456'
				})
				.expect(400);
			expect(error.type).to.equal('existing-data');
		});
	});
	describe('DELETE /api/v1/artists/:id', function () {
		it('should delete an artist', async () => {
			const response = await server
				.delete('/api/v1/artists/ART-456')
				.expect(204)
			expect(response, undefined);
			const { body: artist } = await server
				.delete('/api/v1/artists/ART-456')
				.set('Accept', 'application/json')
				.expect(404)
		});
		it('should not found the artist', async () => {
			await server
				.put('/api/v1/artists/ART-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});

	describe('GET /api/v1/artists/:id/disks', function () {
		it('Recover one artist\'s disks', async () => {
			const { body: disks } = await server
				.get('/api/v1/artists/ART-123/disks')
				.set('Accept', 'application/json')
				.expect(200)
			expect(disks.length).to.equal(1)
		});
		it('should not recover any artist', async () => {
			await server
				.get('/api/v1/artists/ART-121/disks')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
	describe('GET /api/v1/artists/:id/songs', function () {
		it('Recover one artist\'s songs', async () => {
			const { body: songs } = await server
				.get('/api/v1/artists/ART-123/songs')
				.set('Accept', 'application/json')
				.expect(200)
			expect(songs.length).to.equal(1)
			
		});
		it('should not recover any artist', async () => {
			await server
				.get('/api/v1/artists/ART-121/songs')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
});