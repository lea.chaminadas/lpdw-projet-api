'use strict';

const { expect } = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixturesDisks = require('../fixtures/disks')
const fixturesArtists = require('../fixtures/artists')
const fixturesSongs = require('../fixtures/songs')

const server = request(createServer());

describe('Disk api', function () {
	before(async function () {
		await database.sequelize.query('DELETE from Artists');
        await database.sequelize.query('DELETE from Disks');
        await database.sequelize.query('DELETE from Songs');
        
		const { Disks, Artists, Songs } = database;
        
        const promisesArtists = fixturesArtists.map(artist => Artists.create(artist));
        await Promise.all(promisesArtists)

		const promisesDisks = fixturesDisks.map(disk => Disks.create(disk));
		await Promise.all(promisesDisks);
		
		const promisesSongs = fixturesSongs.map(song => Songs.create(song));
		await Promise.all(promisesSongs);
	});

	// /api/v1/disks
	describe('GET /api/v1/disks', function () {
		it('Recover all disks', async () => {
			const { body: disks } = await server
				.get('/api/v1/disks')
				.set('Accept', 'application/json')
				.expect(200);
			expect(disks).to.be.an('array');
			expect(disks.length).to.equal(2);
		});
		it('Filtering disks', async () => {
			const { body: disks } = await server
				.get('/api/v1/disks')
				.query({
					genre: 'Pop'
				})
				.set('Accept', 'application/json')
				.expect(200);

			expect(disks).to.be.an('array');
			expect(disks.length).to.equal(1);
			expect(disks[0].id).to.equal('DIS-123');
		});
	});

	describe('POST /api/v1/disks', function () {
		it('should create an disk', async () => {
			const { body: disk } = await server
				.post('/api/v1/disks')
				.set('Accept', 'application/json')
				.send({
					id: 'DIS-101112',
					title: 'Thriller',
					artistId: 'ART-123',
					date: '2019-01-01',
                    numberOfTracks: 20,
                    totalDuration: '45:45',
					genre: 'Pop'
				})
				.expect(201);

			expect(disk).to.be.an('object');
			expect(disk.id).to.equal('DIS-101112');
			expect(disk.artistId).to.equal('ART-123');
		});
		it('should not create an disk and send a bad request error  because ID is already used', async () => {
			const {body: error} = await server
				.post('/api/v1/disks')
				.set('Accept', 'application/json')
				.send({
					id: 'DIS-101112',
					title: 'Thriller',
					artistId: 'ART-123',
					date: '2019-01-01',
                    numberOfTracks: 20,
                    totalDuration: '45:45',
					genre: 'Pop'
				})
				.expect(400);
			expect(error.type).to.equal('existing-data');
		});
		it('should not create an artist and send a bad request error if data are missing', async () => {
			const { body: error } = await server
				.post('/api/v1/disks')
				.set('Accept', 'application/json')
				.send({
					id: 'DIS-131415',
					title: 'Thriller',
					artistId: 'ART-123',
					date: '2019-01-01',
					numberOfTracks: 20,
					totalDuration: '45:45'
					// missing genre
				})
				.expect(400);
			expect(error.type).to.equal('missing-data');
		});
		it('should not create an artist and send a bad request error if linked artist does not exist', async () => {
			const { body: error } = await server
				.post('/api/v1/disks')
				.set('Accept', 'application/json')
				.send({
					id: 'DIS-161718',
					title: 'Thriller',
					artistId: 'ART-100',
					date: '2019-01-01',
					numberOfTracks: 20,
					totalDuration: '45:45',
					genre: 'Pop'
				})
				.expect(400);
			expect(error.type).to.equal('invalid-data');
		});
	});
	describe('GET /api/v1/disks/:id', function () {
		it('Recover one disk by its id', async () => {
			const { body: disk } = await server
				.get('/api/v1/disks/DIS-123')
				.set('Accept', 'application/json')
				.expect(200)
			expect(disk.genre, 'Pop');
		});
		it('should not recover any disk', async () => {
			await server
				.get('/api/v1/disks/DIS-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
	describe('PUT /api/v1/disks/:id', function () {
		it('should update an disk', async () => {
			const { body: disk } = await server
				.put('/api/v1/disks/DIS-123')
				.set('Accept', 'application/json')
				.send({
					genre: 'Pop/Rock'
				})
				.expect(200)
			expect(disk.genre, 'Pop/Rock');
		});
		it('should not found the disk', async () => {
			await server
				.put('/api/v1/disks/DIS-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
		it('should not update an disk and send a bad request error  because ID is already used', async () => {
			const {body: error} = await server
				.put('/api/v1/disks/DIS-123')
				.set('Accept', 'application/json')
				.send({
					id: 'DIS-101112'
				})
				.expect(400);
			expect(error.type).to.equal('existing-data');
		});
		it('should not update an artist and send a bad request error if data are missing', async () => {
			const { body: error } = await server
				.put('/api/v1/disks/DIS-123')
				.set('Accept', 'application/json')
				.send({
					totalDuration: ''
				})
				.expect(400);
			expect(error.type).to.equal('missing-data');
		});
		it('should not update an artist and send a bad request error if linked artist does not exist', async () => {
			const { body: error } = await server
				.put('/api/v1/disks/DIS-123')
				.set('Accept', 'application/json')
				.send({
					artistId: 'ART-100',
				})
				.expect(400);
			expect(error.type).to.equal('invalid-data');
		});
	});
	describe('DELETE /api/v1/disks/:id', function () {
		it('should delete an disk', async () => {
			const response = await server
				.delete('/api/v1/disks/DIS-123')
				.expect(204)
			expect(response, undefined);
		});
		it('should not found the disk', async () => {
			await server
				.put('/api/v1/disks/DIS-121')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});

	// /api/v1/disks/:id/songs
	describe('GET /api/v1/disks/:id/songs', function () {
		it('Recover one artist\'s songs', async () => {
			const { body: songs } = await server
				.get('/api/v1/disks/DIS-456/songs')
				.set('Accept', 'application/json')
			expect(songs.length).to.equal(1)
		});
		it('should not recover any artist', async () => {
			await server
				.get('/api/v1/disks/DIS-121/songs')
				.set('Accept', 'application/json')
				.expect(404)
		});
	});
});